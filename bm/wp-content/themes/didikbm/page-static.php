<?php
/*
Template Name: Frontpage
*/
?>

<?php get_header(); ?>

		<div id="slider">
			<div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="5000" data-cycle-slides="> div">
			    <!-- prev/next links -->
			    <span class="cycle-prev"></span>
			    <span class="cycle-next"></span>
			    <div><a href="?page_id=631"><img src="<?php bloginfo( 'template_url' ); ?>/images/Daftar-calon.jpg"></a></div>
			    <div><img src="<?php bloginfo( 'template_url' ); ?>/images/banner-4.jpg"></div>
			    <div><a href="?page_id=23"><img src="<?php bloginfo( 'template_url' ); ?>/images/banner1.png"></a></div>
			    <div><img src="<?php bloginfo( 'template_url' ); ?>/images/banner2.png"></div>
			</div>
		</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>