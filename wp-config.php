<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dm');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';(O<.}s*(mt?>VwEGvq)Sxg4z~]3z-!TvI,$DlG+:@~L(%@p^LR-`f|;]yJW WOL');
define('SECURE_AUTH_KEY',  '-1I)5[s]Wn|[VUurJEV8uozNA_Gw$Hkc:jk~(]d&{2 j+^-fX=YU5P%3=t*gZ&iL');
define('LOGGED_IN_KEY',    '9eAy1*8Qh@85_IF9h$Qk0/B2 5uG<pS%c)h!-|5p(qo|M*,`&=#FYBztt@IRA;fa');
define('NONCE_KEY',        '9Qy05kFRQZd9%zY/^&IK]*IV|~Y$i,RtBcxY5|51rX#%V/p>Nz[)s^:qtmT&9*u,');
define('AUTH_SALT',        '9?/t>d,I.#4wu(sE T[qNUuP]VSw9 Z)gdou&4e>$>tf$*&4tb^@,f=NX_|@UMlg');
define('SECURE_AUTH_SALT', 'lsF /u|QsD0x@m@Ldfz{o$S<`0l7V$e!}3P8L5Ymdx S9yWrKm}--m+m(LJ[qT=x');
define('LOGGED_IN_SALT',   'wHjxG/X!1DEfVV3E|/n/* x%ZT-(p8w],=N$a0p&.-pfARn+!fT/DJ/)WfL-kj]#');
define('NONCE_SALT',       'AA=IGKbF}s:U+9L(0QvZUS{eNait.[q}68Gq5mVz8YPVT#497K.:F8[jDV!7R/QQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dmc_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
