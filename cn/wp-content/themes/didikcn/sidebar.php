<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

$options = twentyeleven_get_theme_options();
$current_layout = $options['theme_layout'];

if ( 'content' != $current_layout ) :
?>
	</div><!-- #main -->
	<div class="btm-nav cf" id="navigation">
		<?php wp_nav_menu( array('theme_location'  => 'bottom' )); ?>
	</div>
<?php endif; ?>