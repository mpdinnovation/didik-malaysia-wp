<?php
/*
Template Name: Frontpage
*/
?>

<?php get_header(); ?>

		<div id="slider">
			<div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="5000" data-cycle-slides="> div">
			    <!-- prev/next links -->
			    <span class="cycle-prev"></span>
			    <span class="cycle-next"></span>
			    <div><img src="<?php bloginfo( 'template_url' ); ?>/images/banner6.jpg"></div>
			    <div><a href="?page_id=474"><img src="<?php bloginfo( 'template_url' ); ?>/images/banner7en.jpg"></a></div>
			    <div><img src="<?php bloginfo( 'template_url' ); ?>/images/banner4.png"></div>
			    <div><a href="?page_id=23"><img src="<?php bloginfo( 'template_url' ); ?>/images/banner1.png"></a></div>
			    <div><img src="<?php bloginfo( 'template_url' ); ?>/images/banner2.png"></div>
			    <div><a href="?page_id=6"><img src="<?php bloginfo( 'template_url' ); ?>/images/banner3.png"></a></div>
			</div>
		</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>