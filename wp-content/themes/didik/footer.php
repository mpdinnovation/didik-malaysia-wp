<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

	<div id="icon" class="cf">
		<img src="<?php bloginfo( 'template_url' ); ?>/images/icon2.png" />
	</div>
	<div id="btm-wrapper">
		<div id="footer">
			<?php wp_nav_menu( array('theme_location'  => 'footer' )); ?> 
			<!-- <ul>
				<li><a href="news.html">In The News</a></li>
				<li><a href="first100.html">The First 100 Days</a></li>
				<li><a href="achievements.html">Our Achievements</a></li>
				<li><a href="#">Video Gallery</a></li>
				<li><a href="#">For Students</a></li>
				<li><a href="#">Malaysian Education Blueprint</a></li>
			</ul> -->
			<p>Copyright &#169; 2013. All Rights Reserved.</p>
		</div>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25361124-19', 'didikmalaysia.my');
  ga('send', 'pageview');

</script>
</body>
</html>