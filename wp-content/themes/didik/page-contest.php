<?php
/*
Template Name: Contest
*/
?>
<style type="text/css">
#navigation {
	display: none;
}
</style>

<?php get_header(); ?>
<div id="contest-page">
	<div class="top-wrap">
		<div class="text-wrap">
			<h1>TEACHERS' DAY</h1>
			<h2>Snap &amp; Upload Contest</h2>
		</div>
		<div class="image-box">
			<span class="hastag">#terimakasihcikgu</span>
		</div>
	</div>
	<div id="social-gallery">
		<div id="gallery-title">
			Winners
		</div>
		<div id="gallery-wrap">
			<iframe width="940" height="529" src="//www.youtube.com/embed/yTKH0oonsaI" frameborder="0" allowfullscreen style="margin-bottom: 20px;"></iframe>
			<img src="<?php bloginfo( 'template_url' ); ?>/images/contest-winner-en.jpg">
		</div>
	</div>
	<!-- <div id="step-by-step">
		<div class="step-title">
			<h2>What you have to do</h2>
		</div>
		<div class="step-wrap">
			<div id="step-1">
				<div class="circle">1</div>
				<div class="small-title">Follow</div>
				<div class="step-detail">
					<div>
						<i class="fa fa-instagram"></i>
						<span>@DidikMalaysia</span>
					</div>
					<div>
						<i class="fa fa-twitter"></i>
						<span>@DidikMalaysia</span>
					</div>
					<div>
						<i class="fa fa-envelope"></i>
						<span>didikmalaysia@gmail.com</span>
					</div>
				</div>
			</div>
			<div id="step-2">
				<div class="circle">2</div>
				<div class="small-title">snap &amp; upload</div>
				<div class="step-detail">
					Upload a photo of yourself and your teacher
				</div>
			</div>
			<div id="step-3">
				<div class="circle">3</div>
				<div class="small-title">Caption &amp; Tag</div>
				<div class="step-detail">
					Add a caption and tag <br>#terimakasihcikgu <br>For email, include #terimakasihcikgu on subject (Caption: max 20 words)
				</div>
			</div>
		</div>
		<div id="contest-prize">
			<div class="small-title">PRIZES</div>
			<div class="step-detail">
				Grab the chance to win main prize consist of 2 iPad Mini and 2 external hard disk for teacher and student, and 10 pairs iPod Shuffle as consolation prizes.
				<div id="warning">* Terms and conditions apply</div>
				<div id="warning">** Please set your account to <strong>PUBLIC</strong></div>
			</div>
		</div>
	</div>

	<div id="tnc">
		<div class="tnc-title">
			<h2>Terms &amp; Conditions</h2>
		</div>
		<div class="tnc-detail">
			<ol>
				<li>
					The #terimakasihcikgu Instagram Contest (“Contest”) is organised by Ministry of  Education (MOE) and will run from 9 May 2014 – 16 May 2014 (“Contest  Period").
				</li>
				<li>
					To qualify for the Contest, participants must:
					<ul>
						<li>'Follow' DidikMalaysia Instagram account;</li>
						<li>Employees or immediate family members and relatives of Media Prima staff, its affiliates and subsidiaries are not entitled to participate.</li>
					</ul>
				</li>
				<li>
					The mechanics of the Contest are as follows:
					<ul>
						<li>
							Participants are to upload a (selfie) picture, together with a caption of not more than 20 words in length on their Instagram account. The submission should include:
							<ol style="list-style-type:lower-alpha;">
								<li>Tag to the DidikMalysia Instagram / Twitter account @didikmalaysia</li>
								<li>Hashtag it with #terimakasihcikgu</li>
							</ol>
						</li>
						<li>Winners will be contacted and needs to provide / proof their instagram / twitter account. Failure to do so will lead to disqualification from the Contest.</li>
						<li>Email entries, please indicate #terimakasihcikgu on subject.</li>
					</ul>
				</li>
				<li>
					Winners of the Contest will receive the following prizes (“Prizes”):
					<ul>
						<li>First Prize – 2 x Ipad Mini (student and teacher)</li>
						<li>Consolation Prize - 10 pairs of RM150 vouchers (student and teacher)</li>
					</ul>
				</li>
				<li>
					The Participant represents and warrants that he/she is the owner of the picture  and/or video and/or music ‘the material’ submitted to DidikMalaysia and has 	the authority to use the material  and is not in breach of any intellectual property rights of any party. The Participant fully indemnifies  MOE and MPB of any claim and/or liability arising out of any infringement or alleged infringement of any copyrighted material or other intellectual property by the Participant. The obligation of this clause shall survive the expiration of this Contest period. 
				</li>
				<li>
					All pictures and/or videos submitted to DidikMalysia shall become the sole property of MOE and MPB and may be used or reproduced for any purposes not limited to any marketing or promotional purpose. The Participant shall hold MOE and MPB free and harmless from any claim and/or liability whatsoever resulting from the usage or the reproduction of the submitted picture / video. The obligation of this clause shall survive the expiration of this Contest period.
				</li>
				<li>
					MOE and MPB reserves the right to disqualify, at its sole and absolute discretion, any Participant and/or revoke or forfeit any Prize at any stage of the Contest if:- 
					<ul>
						<li>The participant is not an Eligible Participant;</li>
						<li>Entry/Entries is/are incomplete;</li>
						<li>Breach of any of the Terms and Conditions or other rules and regulations of the Contest or violates any applicable laws or regulations; </li>
						<li>MOE and MPB has, at its sole discretion, any reason whatsoever to believe that  such Eligible Participant has attempted to undermine the operation of the Contest in any way whatsoever, including but not limited to fraud, cheating  or deception.</li>
					</ul>
				</li>
				<li>
					MOE and MPB ’ decision on matters relating to the Contest (including, but not limited to the selection of the winners) shall be final and absolute. No discussion, correspondence, enquiry, appeal or challenge by any of the Eligible Participants in respect of any decision of MOE and MPB shall be entertained.
				</li>
				<li>
					The Prizes shall be given in accordance with the mode to be specified by MOE and MPB, at its sole and absolute discretion, upon notification of any winnings and within a stipulated time period.
				</li>
				<li>
					Notwithstanding the above, MOE and MPB is entitled to replace the Prizes with other prizes of similar value at any time without any prior notice.
				</li>
				<li>
					The Prize cannot be exchanged or sold for cash and is non-transferable to any other person.
				</li>
				<li>
					MOE and MPB reserves the right to cancel, terminate or suspend the Contest without any prior notice. For the avoidance of doubt, any cancellation, termination or suspension by MOE and MPB of the Contest shall not entitle the Eligible Participants to any claim or compensation against MOE and MPB, its affiliates, subsidiaries, employees, representatives, retailers, distributors, dealers as well as its advertising, creative, media, digital and design for any and all losses or damages suffered or incurred by the Participants as a direct or an indirect result of the act of cancellation, termination or suspension thereof. 
				</li>
				<li>
					MOE and MPB shall not be liable for any disruption to the Contest, whether due to technical problems or otherwise, which is beyond its reasonable control. In the event of disruption to the Contest, reasonable efforts shall be used to rectify the disruption and resume the Contest on a fair and equitable basis to the Participants.
				</li>
				<li>
					MOE and MPB, its affiliates, subsidiaries, employees, representatives, retailers, distributors, dealers as well as its advertising, creative, media, digital, design and other creative services, shall not be liable for any loss or damage whatsoever suffered (including but not limited to loss of opportunities and any indirect or consequential loss) or personal injury suffered or sustained in connection with or arising from either participation in this Contest or with any of the Prizes offered or forfeited.
				</li>
				<li>
					The Terms and Conditions of the Contest shall be construed, governed and interpreted in accordance with the laws of Malaysia.
				</li>
				<li>
					All rights and privileges herein granted to MOE and MPB are irrevocable and not subjected to rescission, restraint or injunction under any and all circumstances. Under no circumstances shall the Participants have the right to injunctive relief or to restrain or otherwise interfere with the organization of the Contest, the production, distribution, exhibition and/or exploitation of the Contest and/or any product based on and/or derived from the Contest.
				</li>
				<li>
					All personal data collected will only be used for the purposes of managing and operating the Contest and not for any other purpose.  
				</li>
				<li>
					MOE and MPB reserves the right at its absolute discretion to vary, delete or add to any of these Terms and Conditions without any prior notice.
				</li>
				<li>
					These Terms and Conditions will prevail over any inconsistent terms, conditions, provisions or representations contained in any other promotional material advertising the Contest. 
				</li>
				<li>
					The main language of the Terms and Conditions shall be the English language. Any translation to any language other than English shall be for convenience only.
				</li>
				<li>
					By virtue of acceptance of the Contest prize, the Winners signify their absolute and unconditional acceptance and agreement to all the Terms and Conditions stipulated herein.
				</li>
				<li>
					MOE and MPB, its agents and employees shall not be liable for any loss or 	damage whatsoever suffered (including but not limited to indirect or consequential losses) or personal injury suffered or sustained in connection with or arising from either participation in this Contest or with any of the prizes offered or forfeited.
				</li>
				<li>
					By participating in this Contest, it is deemed that the Participants have read and agreed to be bound by these Terms and Conditions, General Terms and Conditions which can be viewed at www.didikmalaysia.com.my. Any breach of these Terms and Conditions may, at MOE and MPB’s absolute discretion, result in forfeiture of any of the prizes.
				</li>
			</ol>
		</div>
	</div> -->
</div>
<?php get_footer(); ?>