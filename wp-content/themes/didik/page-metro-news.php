<?php
/*
Template Name: Metro news
*/
?>

<?php get_header(); ?>

		<div id="primary">
			<div id="content" role="main">
				<div id="news">
					<div id="content-wrapper">

					<?php
					$temp = $wp_query;
					$wp_query= null;
					$wp_query = new WP_Query();
					$wp_query->query('showposts=10'.'&paged='.$paged.'&category_name=hmetro');
					?>
					<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<?php global $more; $more = 0; ?>

						<article>
		                  <h4><a href="<?php the_permalink() ?>" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
		                  <?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
					  		<?php the_content( __( 'Continue reading... ', 'didik' ) ); ?>
						  	
						  		
						  	<?php endif; ?>
					  	
					  	</article>
						

						<?php endwhile; // end of one post ?>

					    <!-- Previous/Next page navigation -->
					    <div class="page-nav">
						    <?php paginate(); ?>
					    </div>    

					<?php $wp_query = null; $wp_query = $temp;?>
					</div>
			    </div>
			</div>
		</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>