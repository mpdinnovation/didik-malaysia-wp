<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */



get_header(); ?>
<script type="text/javascript">
	Shadowbox.init();
	</script>
	<div id="content">
		<div id="news">

		<h2>In The News</h2>
		<h3>PRINT</h3>
        	<div class="column1">
        		<div class="images"><img alt="" src="wp-content/themes/didik/images/nst.png"></div>
		          <?php query_posts('category_name=NST&amp;showposts=5'); ?>
					<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); if( $post->ID == $do_not_duplicate ) continue; update_post_caches($posts); 
		                /* start the loop */ ?>

		                <article>
		                  <h4><a href="<?php the_permalink() ?>" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
		                  <?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
					  		<?php the_content( __( 'Continue reading... ', 'didik' ) ); ?>
						  	
						  		
						  	<?php endif; ?>
					  	
					  	</article>
		              <?php endwhile; ?>
		            <?php else : ?>

			            <p>There are no post at the moment.</p>
			        
			        <?php endif; ?>
		          
		          <a href="?page_id=88 " class="view-more">View More</a>
			</div>

			<div class="column2">
        		<div class="images"><img alt="" src="wp-content/themes/didik/images/bh.png"></div>
		          <?php query_posts('category_name=BH&amp;showposts=5'); ?>
					<?php if (have_posts()) : ?>
		              <?php while (have_posts()) : the_post(); if( $post->ID == $do_not_duplicate ) continue; update_post_caches($posts); 
		                /* start the loop */ ?>

		                <article>
		                  <h4><a href="<?php the_permalink() ?>" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
		                  <?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
					  		<?php the_content( __( 'Continue reading... ', 'didik' ) ); ?>
						  	
						  		
						  	<?php endif; ?>
					  	
					  	</article> 
		              <?php endwhile; ?>
		            <?php else : ?>

		            <p>There are no post at the moment.</p>
		        
		          <?php endif; ?>
		          
		          <a href="?page_id=84" class="view-more">View More</a>
			</div>

			<div class="column3">
        		<div class="images"><img alt="" src="wp-content/themes/didik/images/metro.png"></div>
		          <?php query_posts('category_name=hmetro&amp;showposts=5'); ?>
					<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); if( $post->ID == $do_not_duplicate ) continue; update_post_caches($posts); 
		                /* start the loop */ ?>

		                <article>
		                  <h4><a href="<?php the_permalink() ?>" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
		                  <?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
					  		<?php the_content( __( 'Continue reading... ', 'didik' ) ); ?>
						  	
						  		
						  	<?php endif; ?>
					  	
					  	</article>   
		              <?php endwhile; ?>
		            <?php else : ?>

			            <p>There are no post at the moment.</p>
			        
			        <?php endif; ?>
		          
		          <a href="?page_id=86" class="view-more">View More</a>
			</div>

			<h3>TV</h3>
			<div class="video-container">
				<div class="img-column1">
					<a style="background: url('wp-content/themes/didik/images/image1.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/v/hN9IrspFJxg?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI 18 March 2013</span></a> 

					<a style="background: url('wp-content/themes/didik/images/video6.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/zaJF9hEp-UY?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI 22 March 2013</span></a>

					<a style="background: url('wp-content/themes/didik/images/8tv-may-14.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/yMIlwg-w1FY?rel=0&amp;autoplay=1" rel="shadowbox"><span>8TV May 14</span></a>

					<a style="background: url('wp-content/themes/didik/images/majalah3-may-25.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/RvsCIFzMLrQ?rel=0&amp;autoplay=1" rel="shadowbox"><span>Majalah 3 May 25</span></a>

					<a style="background: url('wp-content/themes/didik/images/NLKO-apr-5.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/zeBGcllm_FE?rel=0&amp;autoplay=1" rel="shadowbox"><span>NLKO Apr 5</span></a>

					<a style="background: url('wp-content/themes/didik/images/MHI_May1-May9.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/cElDxAS1iSg?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI May 1 - May 9</span></a>

					<a style="background: url('//i.ytimg.com/vi_webp/C9QH9xhuQrg/mqdefault.webp') center top no-repeat; background-size:180px;" href="http://www.youtube.com/embed/C9QH9xhuQrg?rel=0&amp;autoplay=1" rel="shadowbox"><span>Pendidik Anak Pribumi (Part 1)</span></a>
				</div>

				<div class="img-column2">
					<a style="background: url('wp-content/themes/didik/images/image2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/v9CgIDLPqQU?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI 19 March 2013</span></a> 

					<a style="background: url('wp-content/themes/didik/images/8tv-may-15.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/MBJxQXzLcWY?rel=0&amp;autoplay=1" rel="shadowbox"><span>8TV May 15</span></a>

					<a style="background: url('wp-content/themes/didik/images/MOE-guru-buta.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/H3cFwEL7kck?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE Guru Buta</span></a>

					<a style="background: url('wp-content/themes/didik/images/NLKO-apr-7.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/8fomJjOQwHk?rel=0&amp;autoplay=1" rel="shadowbox"><span>NLKO Apr 7</span></a>

					<a style="background: url('wp-content/themes/didik/images/video9.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/WgZmewGZAoY?rel=0&amp;autoplay=1" rel="shadowbox"><span>News MHI</span></a>

					<a style="background: url('wp-content/themes/didik/images/MHI_May12-May30.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/g0NLNl0hXuo?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI May 12 - May 30</span></a>

					<a style="background: url('//i.ytimg.com/vi_webp/KX9zCphV5Cs/mqdefault.webp') center top no-repeat; background-size:180px;" href="http://www.youtube.com/embed/KX9zCphV5Cs?rel=0&amp;autoplay=1" rel="shadowbox"><span>Pendidik Anak Pribumi (Part 2)</span></a>
				</div>

				<div class="img-column3">
					<a style="background: url('wp-content/themes/didik/images/image3.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/49orQ1g7uIM?rel=0&amp;autoplay=1" rel="shadowbox"><span>NLKO 15 March 2013</span></a> 

					<a style="background: url('wp-content/themes/didik/images/buletin-utama-may-10.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/MBJxQXzLcWY?rel=0&amp;autoplay=1" rel="shadowbox"><span>Buletin Utama May 10</span></a>

					<a style="background: url('wp-content/themes/didik/images/MOE-guru-cekal.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/gBrk-SVQ_7o?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE Guru Cekal</span></a>

					<a style="background: url('wp-content/themes/didik/images/NLKO-apr-13.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/d21SaJ32Gig?rel=0&amp;autoplay=1" rel="shadowbox"><span>NLKO Apr 13</span></a>

					<a style="background: url('wp-content/themes/didik/images/video8.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/IZeMso9nv78?rel=0&amp;autoplay=1" rel="shadowbox"><span>News</span></a>

					<a style="background: url('wp-content/themes/didik/images/mhi-june-part1.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/SVaJ70X2Hus?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI June Part 1</span></a>
				</div>

				<div class="img-column4">
					<a style="background: url('wp-content/themes/didik/images/video4.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/qfU2KV5p5sI?rel=0&amp;autoplay=1" rel="shadowbox"><span>Majalah3 30 March 2013</span></a>

					<a style="background: url('wp-content/themes/didik/images/buletin-utama-may-13.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/VgGwLWJ3qE4?rel=0&amp;autoplay=1" rel="shadowbox"><span>Buletin Utama May 13</span></a>

					<a style="background: url('wp-content/themes/didik/images/MOE-linus.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/VRlIVBFhtHM?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE Linus</span></a>

					<a style="background: url('wp-content/themes/didik/images/nona-apr-21.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/geBzqvwLZbM?rel=0&amp;autoplay=1" rel="shadowbox"><span>NONA Apr 21</span></a>

					<a style="background: url('wp-content/themes/didik/images/video7.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/CiN3_IqXNwA?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI NLKO</span></a>

					<a style="background: url('wp-content/themes/didik/images/mhi-june-part2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/l4FzkOkFGpE?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI June Part 2</span></a>

				</div>

				<div class="img-column5">
					<a style="background: url('wp-content/themes/didik/images/video5.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/ElA45rxKtkc?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI 27 March 2013</span></a>

					<a style="background: url('wp-content/themes/didik/images/7Edition-may-15.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/UBRWjZPQv-I?rel=0&amp;autoplay=1" rel="shadowbox"><span>7Edition May 15</span></a>

					<a style="background: url('wp-content/themes/didik/images/buletin-utama-may-15.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/PwBUGMXLIJs?rel=0&amp;autoplay=1" rel="shadowbox"><span>Buletin Utama May 15</span></a>

					<a style="background: url('wp-content/themes/didik/images/MOE-tv.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/7nobC7S5C6c?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE Transformasi Vokasional</span></a>

					<a style="background: url('wp-content/themes/didik/images/MPPK.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/gADGMKjyQBM?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE MHI Interview</span></a>

					<a style="background: url('wp-content/themes/didik/images/mhi-july.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/6ud0RUr4YhE?rel=0&amp;autoplay=1" rel="shadowbox"><span>MHI July</span></a>

				</div>


			</div>
			<h3>Radio</h3>
			<div class="video-container">
				<div class="img-column1">
					
					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/9nOs1tfHNi0?rel=0&amp;autoplay=1" rel="shadowbox"><span>Radio Ad 1</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/qHe9pjjgVo?rel=0&amp;autoplay=1" rel="shadowbox"><span>Reasons why we need PPPM Radio Ad (BM)</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/rhGFOdBV2Xk?rel=0&amp;autoplay=1" rel="shadowbox"><span>KPM CN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/2sm0QKANKzc?rel=0&amp;autoplay=1" rel="shadowbox"><span>Upskilling Teacher EN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/H9XrQoOqz0o?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE - Sambutan Hari Guru EN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/qM-PhSnT9Ls?rel=0&amp;autoplay=1" rel="shadowbox"><span>Fly FM - TEACHER'S DAY WISHES 2014</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/ozL2b4p9Lrc?rel=0&amp;autoplay=1" rel="shadowbox"><span>Hot FM Talkset</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/ZhO6n0zUVUk?rel=0&amp;autoplay=1" rel="shadowbox"><span>LINUS</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/OCom-UAucks?rel=0&amp;autoplay=1" rel="shadowbox"><span>UPSR Resit 2014 CHI</span></a>

				</div>
				<div class="img-column2">
					
					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/ZdVvgekDtWs?rel=0&amp;autoplay=1" rel="shadowbox"><span>Radio Ad 2</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/zwTsfzZtAS4?rel=0&amp;autoplay=1" rel="shadowbox"><span>Reasons why we need PPPM Radio Ad (EN)</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/bXDvVxyZHzU?rel=0&amp;autoplay=1" rel="shadowbox"><span>Program I Think BM</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/vCpAO5qEh0Y?rel=0&amp;autoplay=1" rel="shadowbox"><span>Upskilling Teacher CN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/yGTMhhNA3VE?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE - Sambutan Hari Guru BM</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/niWEC0aOCQ8?rel=0&amp;autoplay=1" rel="shadowbox"><span>Fly FM Teacher's Day TA</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/GBnI36sGi2Q?rel=0&amp;autoplay=1" rel="shadowbox"><span>One FM PSA - teachers day</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/bTUdFjIFyjg?rel=0&amp;autoplay=1" rel="shadowbox"><span>PRIME</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/0WG0PH_CoeQ?rel=0&amp;autoplay=1" rel="shadowbox"><span>UPSR Resit 2014</span></a>

				</div>
				<div class="img-column3">

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/IOJ6eb7gFq0?rel=0&amp;autoplay=1" rel="shadowbox"><span>PPPM Launching Radio Ad (BM)</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/9ogK_jOxiu4?rel=0&amp;autoplay=1" rel="shadowbox"><span>Reasons why we need PPPM Radio Ad (CN)</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/qC23Mu0xjzQ?rel=0&amp;autoplay=1" rel="shadowbox"><span>Program I Think EN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/_ONfsHgA0-Q?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE - Hari Guru EN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/Y1yjG7Nyq9Y?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE - Sambutan Hari Guru CN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/_5uC-qYeDPI?rel=0&amp;autoplay=1" rel="shadowbox"><span>Fly FM teacher's Day talkset</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/apTP80HYOSs?rel=0&amp;autoplay=1" rel="shadowbox"><span>One FM Sweeper</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/aIT39Ro1i38?rel=0&amp;autoplay=1" rel="shadowbox"><span>Oral Proficiency</span></a>
					
				</div>
				<div class="img-column4">

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/7gGF_wI5Jmg?rel=0&amp;autoplay=1" rel="shadowbox"><span>PPPM Launching Radio Ad (EN)</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/LkoxXIb3Hmw?rel=0&amp;autoplay=1" rel="shadowbox"><span>KPM BM</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/Ld9KVGo4PA8?rel=0&amp;autoplay=1" rel="shadowbox"><span>Program I Think CN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/5I5In8i5Gy4?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE - Hari Guru BM</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/a5F-DwkMESE?rel=0&amp;autoplay=1" rel="shadowbox"><span>Fly FM Teacher's Day by Sheikh Muszaphar</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/54KQwvbpMDo?rel=0&amp;autoplay=1" rel="shadowbox"><span>Hot FM PSA - HARI GURU 2014</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/MrcR57_6SNM?rel=0&amp;autoplay=1" rel="shadowbox"><span>Hari Guru Commercial (Tamil)</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/OgqcEmulM8s?rel=0&amp;autoplay=1" rel="shadowbox"><span>UPSR Resit 2014 ENG</span></a>
					
				</div>
				<div class="img-column5">

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/lmSdaM5M4-o?rel=0&amp;autoplay=1" rel="shadowbox"><span>PPPM Launching Radio Ad (CN)</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/yHp33m6C82A?rel=0&amp;autoplay=1" rel="shadowbox"><span>KPM EN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/327D5mJpZ2Q?rel=0&amp;autoplay=1" rel="shadowbox"><span>Upskilling Teacher BM</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/alvWEXqRzI4?rel=0&amp;autoplay=1" rel="shadowbox"><span>MOE - Hari Guru CN</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/lcMVnJE5Bfs?rel=0&amp;autoplay=1" rel="shadowbox"><span>Fly FM PSA - TEACHERS DAY 2014</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/STfISRKqoZI?rel=0&amp;autoplay=1" rel="shadowbox"><span>Hot FM sweeper</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/g-W9h9QV6t4?rel=0&amp;autoplay=1" rel="shadowbox"><span>Sambutan Hari Guru Commercial (Tamil)</span></a>

					<a style="background: url('wp-content/themes/didik/images/radio2.jpg') no-repeat; background-size:150px;" href="http://www.youtube.com/embed/8o07JIUH0X8?rel=0&amp;autoplay=1" rel="shadowbox"><span>UPSR Resit 2014 BM</span></a>
					
				</div>
			</div>
		</div>
	</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>